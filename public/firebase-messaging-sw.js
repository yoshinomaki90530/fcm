
console.log("fcmsw.js");
importScripts('https://www.gstatic.com/firebasejs/8.4.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.4.1/firebase-messaging.js');
firebase.initializeApp({
    apiKey: "AIzaSyC1QwnHiw2O3HbxmK8OyZD-3wGMQEK8UW0",
    authDomain: "cloudmessage-20501.firebaseapp.com",
    projectId: "cloudmessage-20501",
    storageBucket: "cloudmessage-20501.appspot.com",
    messagingSenderId: "961170481100",
    appId: "1:961170481100:web:cdadde66791c3554454769",
    measurementId: "G-M0YGRB8E58"
});



const messaging = firebase.messaging();
messaging.onBackgroundMessage(function (payload) {
    console.log('Received background message:', payload);


    const notificationTitle = payload.notification.title;
    const notificationOptions = {
        body: payload.notification.body,
    };


    self.registration.showNotification(notificationTitle, notificationOptions);
});

