import { StrictMode } from "react";
import ReactDOM from "react-dom";
import * as serviceWorkerRegistration from './serviceWorkerRegistration';
import App from "./App";
import firebase from "firebase/app";
import "firebase/messaging";
import firebaseConfig from "./firebase";

const rootElement = document.getElementById("root");
ReactDOM.render(
    <StrictMode>
        <App />
    </StrictMode>,
    rootElement
);
serviceWorkerRegistration.register();
// subscribeUser();


if (!firebase.apps.length) {
    firebase.initializeApp(firebaseConfig);
}

async function getFirebase() {
    const registration = await navigator.serviceWorker.ready;
    const messaging = firebase.messaging();
    messaging.getToken({
        serviceWorkerRegistration: registration,
        vapidKey: 'BJxZH2L0CjVtygfWx9t9OT6lOMAgeJB7a5i9I9_CnvrlUpKGXFTqwBYOyk48sVzeIhwyco37whNY_y_A295Rs08'
    }).then((currentToken) => {
        if (currentToken) {
            // Send the token to your server and update the UI if necessary
            // ...
            console.log('token' + currentToken);
        } else {
            // Show permission request UI
            console.log('No registration token available. Request permission to generate one.');
            // ...
        }
    }).catch((err) => {
        console.log('An error occurred while retrieving token. ', err);
        // ...
    });

    messaging.onMessage(payload => {
        console.log("recieved!", payload)
        const notificationTitle = payload.notification.title;
        const notificationOptions = {
            body: payload.notification.body,
        };
        new Notification(notificationTitle, notificationOptions);
    })
}

getFirebase();
